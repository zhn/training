package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.resource.CountryResource;
import com.gp.training.bizlogic.dao.CountryDao;
import com.gp.training.bizlogic.domain.City;
import com.gp.training.bizlogic.domain.Country;

@Component
public class CountryResourceImpl implements CountryResource {
	
	@Autowired
	private CountryDao countryDao;
	
	@Override
	public CountryDTO getCountry(long countryId) {
		Country country = countryDao.get(Long.valueOf(countryId));	
		return convertCountry2DTO(country);
	}
	
	@Override
	public List<CountryDTO> getCountries() {
		List<Country> countries = countryDao.getAll();
		
		List<CountryDTO> countryDtos = new ArrayList<>(countries.size());
		for (Country country : countries) {
			CountryDTO dto = convertCountry2DTO(country);
			countryDtos.add(dto);
		}
		
		return countryDtos;	
		/*return countries.stream().map(c -> convertCountry2DTO(c)).collect(Collectors.toList());*/
	}
		
	private CountryDTO convertCountry2DTO(Country country) {
		CountryDTO dto = new CountryDTO();
		if (country != null) {
			dto.setId(country.getId());
			dto.setName(country.getName());
			dto.setCode(country.getCode());
		}	
		return dto;
	}
}
