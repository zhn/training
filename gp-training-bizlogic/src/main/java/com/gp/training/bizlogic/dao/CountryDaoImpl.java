package com.gp.training.bizlogic.dao;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.Country;

@Repository
public class CountryDaoImpl extends GenericDaoImpl<Country, Long> implements CountryDao {

	public CountryDaoImpl() {
		super(Country.class);
	}
}
