package com.gp.training.bizlogic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.dao.AvailabilityDao;
import com.gp.training.bizlogic.params.AvailSearchParams;

@Component
public class AvailabilityServiceImpl implements AvailabilityService {
	
	@Autowired
	private AvailabilityDao availabilityDao;
	
	@Override
	public List<OfferDTO> findOffers(AvailSearchParams params) {

		return availabilityDao.findOffers(params);
	}
}
