package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.RoomType;

public interface RoomTypeDao extends GenericDao<RoomType, Long> {

}
