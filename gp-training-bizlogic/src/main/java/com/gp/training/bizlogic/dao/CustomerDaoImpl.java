package com.gp.training.bizlogic.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.Customer;

@Repository
public class CustomerDaoImpl extends GenericDaoImpl<Customer, Long> implements CustomerDao {

	protected CustomerDaoImpl() {
		super(Customer.class);
	}

}
