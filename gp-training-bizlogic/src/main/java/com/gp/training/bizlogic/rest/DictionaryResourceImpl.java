package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;
import com.gp.training.bizlogic.api.resource.DictionaryResource;
import com.gp.training.bizlogic.dao.CityDao;
import com.gp.training.bizlogic.dao.CountryDao;
import com.gp.training.bizlogic.dao.MealTypeDao;
import com.gp.training.bizlogic.dao.RoomTypeDao;
import com.gp.training.bizlogic.domain.City;
import com.gp.training.bizlogic.domain.Country;
import com.gp.training.bizlogic.domain.MealType;
import com.gp.training.bizlogic.domain.RoomType;

@Component
@Path("/dictionary")
public class DictionaryResourceImpl implements DictionaryResource{

	@Autowired
	private CountryDao countryDao;

	@Autowired
	private CityDao cityDao;

	@Autowired
	private MealTypeDao mealTypeDao;

	@Autowired
	private RoomTypeDao roomTypeDao;

	@GET
	@Path("/roomtype/{roomTypeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public RoomTypeDTO getRoomTypeById(@PathParam("roomTypeId") Long roomTypeId) {
		RoomType roomType = roomTypeDao.get(roomTypeId);
		return convertRoomTypeToDTO(roomType);
	}

	@GET
	@Path("/roomtype")
	@Produces(MediaType.APPLICATION_JSON)
	public List<RoomTypeDTO> getAllRoomTypes() {
		List<RoomType> roomTypes = roomTypeDao.getAll();
		List<RoomTypeDTO> roomTypeDtos = new ArrayList<>(roomTypes.size());
		for (RoomType roomType : roomTypes) {
			RoomTypeDTO dto = convertRoomTypeToDTO(roomType);
			roomTypeDtos.add(dto);
		}
		return roomTypeDtos;
	}

	@GET
	@Path("/mealtype/{mealTypeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public MealTypeDTO getMealTypeById(@PathParam("mealTypeId") Long mealTypeId) {
		MealType mealType = mealTypeDao.get(mealTypeId);
		return convertMealTypeToDTO(mealType);
	}

	@GET
	@Path("/mealtype")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MealTypeDTO> getAllMealTypes() {
		List<MealType> mealTypes = mealTypeDao.getAll();
		List<MealTypeDTO> mealTypeDtos = new ArrayList<>(mealTypes.size());
		for (MealType mealType : mealTypes) {
			MealTypeDTO dto = convertMealTypeToDTO(mealType);
			mealTypeDtos.add(dto);
		}
		return mealTypeDtos;
	}

	@GET
	@Path("/city/{cityId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CityDTO getCityById(@PathParam("cityId") Long cityId) {
		City city = cityDao.get(cityId);
		return convertCityToDTO(city);
	}

	@GET
	@Path("/city")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityDTO> getAllCities() {
		List<City> cities = cityDao.getAll();
		List<CityDTO> cityDtos = new ArrayList<>(cities.size());
		for (City city : cities) {
			CityDTO dto = convertCityToDTO(city);
			cityDtos.add(dto);
		}
		return cityDtos;
		
	}

	@GET
	@Path("/country/{countryId}/city")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityDTO> getCitiesByCountryId(@PathParam("countryId") Long countryId){
		List<City> cities = cityDao.getCitiesByCountryId(countryId);
		List<CityDTO> cityDtos = new ArrayList<>(cities.size());
		for (City city : cities) {
			CityDTO dto = convertCityToDTO(city);
			cityDtos.add(dto);
		}
		return cityDtos;
	}
	
	
	@GET
	@Path("/country/{countryId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CountryDTO getCountryById(@PathParam("countryId") Long countryId) {
		Country country = countryDao.get(countryId);
		return convertCountryToDTO(country);
	}

	@GET
	@Path("/country")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountryDTO> getAllCountries() {
		List<Country> countries = countryDao.getAll();
		List<CountryDTO> countryDtos = new ArrayList<>(countries.size());
		for (Country country : countries) {
			CountryDTO dto = convertCountryToDTO(country);
			countryDtos.add(dto);
		}
		return countryDtos;	
	}

	private RoomTypeDTO convertRoomTypeToDTO(RoomType roomType) {
		RoomTypeDTO roomTypeDTO = new RoomTypeDTO();
		if (roomType != null) {
			roomTypeDTO.setId(roomType.getId());
			roomTypeDTO.setName(roomType.getName());
			roomTypeDTO.setCode(roomType.getCode());
			roomTypeDTO.setGuestCount(roomType.getGuestCount());
		}
		return roomTypeDTO;
	}

	private MealTypeDTO convertMealTypeToDTO(MealType mealType) {
		MealTypeDTO mealTypeDTO = new MealTypeDTO();
		if (mealType != null) {
			mealTypeDTO.setId(mealType.getId());
			mealTypeDTO.setName(mealType.getName());
			mealTypeDTO.setCode(mealType.getCode());
		}
		return mealTypeDTO;
	}

	private CityDTO convertCityToDTO(City city) {
		CityDTO cityDTO = new CityDTO();
		if (city != null) {
			cityDTO.setId(city.getId());
			cityDTO.setName(city.getName());
			cityDTO.setCode(city.getCode());
			cityDTO.setCountryDTO(convertCountryToDTO(city.getCountry()));
		}
		return cityDTO;
	}

	private CountryDTO convertCountryToDTO(Country country) {
		CountryDTO countryDTO = new CountryDTO();
		if (country != null) {
			countryDTO.setId(country.getId());
			countryDTO.setName(country.getName());
			countryDTO.setCode(country.getCode());
		}
		return countryDTO;
	}

}
