package com.gp.training.bizlogic.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.Booking;

@Repository
public class BookingDaoImpl extends GenericDaoImpl<Booking, Long> implements BookingDao {

	protected BookingDaoImpl() {
		super(Booking.class);
	}

	@Override
	public Booking getBookingWithGuests(Long bookingId) {

		CriteriaSet criteriaSet = new CriteriaSet();
		criteriaSet.root.fetch("guests");
		criteriaSet.criteriaQuery.select(criteriaSet.root);
		criteriaSet.criteriaQuery.select(criteriaSet.root);
		
		Query query = getEntityManager().createQuery(criteriaSet.criteriaQuery.select(criteriaSet.root)
				.where(criteriaSet.criteriaBuilder.equal(criteriaSet.root.get("id"), bookingId)));
		return (Booking)query.getResultList().get(0);
	}
}
