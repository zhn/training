package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Customer;

public interface CustomerDao extends GenericDao<Customer, Long> {


}
