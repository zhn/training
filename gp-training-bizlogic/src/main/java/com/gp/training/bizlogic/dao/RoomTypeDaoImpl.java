package com.gp.training.bizlogic.dao;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.RoomType;

@Repository
public class RoomTypeDaoImpl extends GenericDaoImpl<RoomType, Long> implements RoomTypeDao {

	protected RoomTypeDaoImpl() {
		super(RoomType.class);
	}

}
