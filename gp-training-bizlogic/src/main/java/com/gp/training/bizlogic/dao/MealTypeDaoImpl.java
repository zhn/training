package com.gp.training.bizlogic.dao;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.MealType;

@Repository
public class MealTypeDaoImpl extends GenericDaoImpl<MealType, Long> implements MealTypeDao {

	protected MealTypeDaoImpl() {
		super(MealType.class);
	}

}
