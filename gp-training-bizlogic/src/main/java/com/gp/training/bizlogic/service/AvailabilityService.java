package com.gp.training.bizlogic.service;

import java.util.List;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.params.AvailSearchParams;

public interface AvailabilityService {
	
	
	public List<OfferDTO> findOffers(AvailSearchParams params);
}
