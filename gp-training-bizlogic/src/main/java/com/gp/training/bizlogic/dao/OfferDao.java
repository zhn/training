package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Offer;

public interface OfferDao extends GenericDao<Offer, Long>{

}
