package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Country;

public interface CountryDao extends GenericDao<Country, Long> {

}
