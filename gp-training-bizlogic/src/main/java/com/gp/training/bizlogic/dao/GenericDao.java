package com.gp.training.bizlogic.dao;

import java.util.List;

public interface GenericDao<T, PK> {

	void save(T entity);

	void update(T entity);

	T get(PK id);

	List<T> getAll();

	public List<T> getAll(int maxResults);
}
