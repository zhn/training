package com.gp.training.bizlogic.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class GenericDaoImpl<T, PK> implements GenericDao<T, PK> {

	private final int DEFAULT_MAX_RESULTS = 100;
	protected final Class<T> entityClass;

	@PersistenceContext
	protected EntityManager entityManager;

	protected GenericDaoImpl(final Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void save(T entity) {
		getEntityManager().persist(entity);
	
	}

	public void update(T entity) {
		getEntityManager().merge(entity);
	}

	public class CriteriaSet {
		public final CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		public final CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		public final Root<T> root = criteriaQuery.from(entityClass);
	}

	public T get(PK id) {
		return getEntityManager().find(entityClass, id);
	}

	public List<T> getAll(int maxResults) {
		CriteriaSet criteriaSet = new CriteriaSet();
		Query query = getEntityManager().createQuery(
				criteriaSet.criteriaQuery.orderBy(criteriaSet.criteriaBuilder.asc(criteriaSet.root.get("id"))));
		return query.setMaxResults(maxResults).getResultList();
	}

	public List<T> getAll() {
		return getAll(DEFAULT_MAX_RESULTS);
	}

}
