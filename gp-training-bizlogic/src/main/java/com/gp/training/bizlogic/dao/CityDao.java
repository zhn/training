package com.gp.training.bizlogic.dao;

import java.util.List;

import com.gp.training.bizlogic.domain.City;

public interface CityDao extends GenericDao<City, Long> {

	List<City> getCitiesByCountryId(Long countryId);

}
