package com.gp.training.bizlogic.dao;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.Offer;

@Repository
public class OfferDaoImpl extends GenericDaoImpl<Offer, Long> implements OfferDao {

	protected OfferDaoImpl() {
		super(Offer.class);
	}
}
