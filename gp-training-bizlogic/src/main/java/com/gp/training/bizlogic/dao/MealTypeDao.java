package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.MealType;

public interface MealTypeDao extends GenericDao<MealType, Long> {

}
