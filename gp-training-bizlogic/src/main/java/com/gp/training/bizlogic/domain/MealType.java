package com.gp.training.bizlogic.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "meal_type")
public class MealType extends AbstractEntity {

	private String name;
	private String code;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
