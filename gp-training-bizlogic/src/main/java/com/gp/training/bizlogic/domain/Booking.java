package com.gp.training.bizlogic.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "booking")
public class Booking extends AbstractEntity {
	@Column(name = "guest_count")
	int guestCount;

	@Column(name = "start_Date")
	Date startDate;

	@Column(name = "end_date")
	Date endDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "offer_id")
	Offer offer;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="booking")
	List<Customer> guests;

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public List<Customer> getGuests() {
		return guests;
	}

	public void setGuests(List<Customer> guests) {
		this.guests = guests;
	}
	
	
}
