package com.gp.training.bizlogic.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.City;

@Repository
public class CityDaoImpl extends GenericDaoImpl<City, Long> implements CityDao {

	protected CityDaoImpl() {
		super(City.class);
	}

	@Override
	public List<City> getCitiesByCountryId(Long countryId) {
		CriteriaSet criteriaSet = new CriteriaSet();
		Query query = getEntityManager().createQuery(
				criteriaSet.criteriaQuery
						.where(criteriaSet.criteriaBuilder.equal(criteriaSet.root.get("country"), countryId))
						.orderBy(criteriaSet.criteriaBuilder.asc(criteriaSet.root.get("id"))));
		return query.getResultList();
	}
}
