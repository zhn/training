package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Booking;

public interface BookingDao extends GenericDao<Booking, Long>{

	Booking getBookingWithGuests(Long bookingId);
	
}
