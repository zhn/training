package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.model.HotelDTO;
import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.dao.BookingDao;
import com.gp.training.bizlogic.dao.CustomerDao;
import com.gp.training.bizlogic.dao.OfferDao;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.Customer;
import com.gp.training.bizlogic.domain.Hotel;
import com.gp.training.bizlogic.domain.Offer;
import com.gp.training.bizlogic.domain.RoomType;
import com.gp.training.bizlogic.utils.DateUtils;

@Component
@Path("/booking")
public class BookingResourceImpl implements BookingResource {

	@Autowired
	private OfferDao offerDao;

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private BookingDao bookingDao;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public BookingDTO create(@QueryParam("offerId") long offerId, @QueryParam("guestCount") int guestCount,
			@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) {
		Booking booking = new Booking();
		Offer offer = offerDao.get(offerId);
		booking.setOffer(offer);
		booking.setStartDate(DateUtils.convertISODateStringToDate(startDate));
		booking.setEndDate(DateUtils.convertISODateStringToDate(endDate));
		booking.setGuestCount(guestCount);

		bookingDao.save(booking);
		BookingDTO bookingDto = convertBookingToDTO(booking);
		return bookingDto;
	}

	@PUT
	@Path("/{bookingId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BookingDTO addGuests(@PathParam("bookingId") int bookingId, List<CustomerDTO> guestsDTO) {
		Booking booking = bookingDao.get(Long.valueOf(bookingId));
		for (CustomerDTO guestDTO : guestsDTO) {
			Customer newCustomer = conveterDtoToCustomer(guestDTO);
			newCustomer.setBooking(booking);
			customerDao.save(newCustomer);
		}
		booking = bookingDao.getBookingWithGuests(Long.valueOf(bookingId));
		BookingDTO bookingDto = convertBookingToDTO(booking);
		return bookingDto;
	}

	@GET
	@Path("/{bookingId}")
	@Produces(MediaType.APPLICATION_JSON)
	public BookingDTO findBookingById(@PathParam("bookingId") int bookingId) {
		Booking booking = bookingDao.getBookingWithGuests(Long.valueOf(bookingId));
		BookingDTO bookingDto = convertBookingToDTO(booking);
		return bookingDto;
	}

	private Customer conveterDtoToCustomer(CustomerDTO customerDTO) {
		Customer customer = new Customer();
		if (customerDTO != null) {
			customer.setFirstName(customerDTO.getFirstName());
			customer.setLastName(customerDTO.getLastName());
			customer.setEmail(customerDTO.getEmail());
		}
		return customer;
	}

	private CustomerDTO conveterCustomerToDto(Customer customer) {
		CustomerDTO customerDTO = new CustomerDTO();
		if (customer != null) {
			if (customer.getId() != null) {
				customerDTO.setId(customer.getId());
			}
			customerDTO.setFirstName(customer.getFirstName());
			customerDTO.setLastName(customer.getLastName());
			customerDTO.setEmail(customer.getEmail());
		}
		return customerDTO;
	}

	private BookingDTO convertBookingToDTO(Booking booking) {
		BookingDTO bookingDTO = new BookingDTO();
		if (booking != null) {
			bookingDTO.setId(booking.getId());
			bookingDTO.setStartDate(booking.getStartDate());
			bookingDTO.setEndDate(booking.getEndDate());
			bookingDTO.setGuestCount(booking.getGuestCount());

			if (booking.getGuests() != null&&booking.getGuests().size()>0) {

				List<Customer> customers = booking.getGuests();
				List<CustomerDTO> customerDtos = new ArrayList<>(customers.size());
				for (Customer customer : customers) {
					CustomerDTO dto = conveterCustomerToDto(customer);
					customerDtos.add(dto);
				}
				bookingDTO.setGuestsDTO(customerDtos);
			}

			OfferDTO offerDTO = new OfferDTO();
			if (booking.getOffer() != null) {
				Offer offer = booking.getOffer();
				offerDTO.setOfferId(offer.getId());
				RoomTypeDTO roomTypeDTO = new RoomTypeDTO();
				if (offer.getRoomType() != null) {
					RoomType roomType = offer.getRoomType();
					roomTypeDTO.setId(roomType.getId());
					roomTypeDTO.setCode(roomType.getCode());
					roomTypeDTO.setName(roomType.getName());
					roomTypeDTO.setGuestCount(roomType.getGuestCount());
				}
				offerDTO.setRoom(roomTypeDTO);

				HotelDTO hotelDTO = new HotelDTO();
				if (offer.getHotel() != null) {
					Hotel hotel = offer.getHotel();
					hotelDTO.setId(hotel.getId());
					hotelDTO.setCode(hotel.getCode());
					hotelDTO.setName(hotel.getName());
				}
				offerDTO.setHotel(hotelDTO);
			}
			bookingDTO.setOfferDTO(offerDTO);
		}
		return bookingDTO;
	}

}
