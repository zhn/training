package com.gp.training.bizlogic.client.resource;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.client.ClientSender;

@Component
public class BookingServiceImpl implements BookingService {

	@Autowired
	private ClientSender sender;

	@Override
	public BookingDTO create(long offerId, int guestCount, String startDate, String endDate) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		BookingResource bookingProxy = webTarget.proxy(BookingResource.class);

		BookingDTO bookingDto = bookingProxy.create(offerId, guestCount, startDate, endDate);
		webTarget.getResteasyClient().close();
		return bookingDto;
	}

	@Override
	public BookingDTO addGuests(int bookingId, List<CustomerDTO> guestsDTO) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		webTarget.getResteasyClient().property("accept", MediaType.APPLICATION_JSON);
		BookingResource bookingProxy = webTarget.proxy(BookingResource.class);
		BookingDTO bookingDto = bookingProxy.addGuests(bookingId, guestsDTO);
		webTarget.getResteasyClient().close();
		return bookingDto;
	}

	@Override
	public BookingDTO findBookingById(int bookingId) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		BookingResource bookingProxy = webTarget.proxy(BookingResource.class);
		BookingDTO bookingDto = bookingProxy.findBookingById(bookingId);
		webTarget.getResteasyClient().close();
		return bookingDto;
	}

}
