package com.gp.training.bizlogic.client.resource;

import java.util.List;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.CustomerDTO;

public interface BookingService {
	BookingDTO create(long offerId, int guestCount, String startDate, String endDate);

	BookingDTO addGuests(int bookingId, List<CustomerDTO> guestsDTO);

	BookingDTO findBookingById(int bookingId);
}
