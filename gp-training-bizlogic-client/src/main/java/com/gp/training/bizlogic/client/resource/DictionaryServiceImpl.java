package com.gp.training.bizlogic.client.resource;

import java.util.List;

import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.resource.DictionaryResource;
import com.gp.training.bizlogic.client.ClientSender;

@Component
public class DictionaryServiceImpl implements DictionaryService {
	
	@Autowired
	private ClientSender sender;
	
	@Override
	public List<CountryDTO> getCountries() {		
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		DictionaryResource dictionaryResourceProxy = webTarget.proxy(DictionaryResource.class);
		
		List<CountryDTO> countries = dictionaryResourceProxy.getAllCountries();
		webTarget.getResteasyClient().close();
		return countries;
	}

	@Override
	public List<CityDTO> getCities() {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		DictionaryResource dictionaryResourceProxy = webTarget.proxy(DictionaryResource.class);
		
		List<CityDTO> cities = dictionaryResourceProxy.getAllCities();
		webTarget.getResteasyClient().close();
		return cities;
	}

	@Override
	public List<CityDTO> getCitiesByCountryId(Long countryId) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		DictionaryResource dictionaryResourceProxy = webTarget.proxy(DictionaryResource.class);
		
		List<CityDTO> cities = dictionaryResourceProxy.getCitiesByCountryId(countryId);
		webTarget.getResteasyClient().close();
		return cities;
	}

}

