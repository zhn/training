-- ������� ����� �������
create table room_type(
	id int(11) primary key  auto_increment, 
	code varchar(10) not null, 
	name varchar(64) not null, 
	guest_count tinyint(1) not null default 0
	) engine=InnoDB default charset=utf8;

-- ���������� room_type
insert into room_type(code, name, guest_count) values ("SNG", "Single", 1), 
	("TWB", "Twin Room", 2),
	("DWB", "Double Room", 2),
	("TRP", "Triple", 3);