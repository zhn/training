CREATE DATABASE  IF NOT EXISTS `training` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `training`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: training
-- ------------------------------------------------------
-- Server version	5.5.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_booking_FK` (`booking_id`),
  CONSTRAINT `customer_booking_FK` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'MSQ','Minsk',1),(2,'GRO','Grodno',1),(3,'PAR','Paris',2),(4,'NIC','Nice',2),(5,'LQN','Lyon',2),(6,'ROM','Rome',3),(7,'MLN','Milan',3),(8,'NPL','Neapol',3);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offer`
--

DROP TABLE IF EXISTS `offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `room_type_id` (`room_type_id`),
  CONSTRAINT `offer_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `offer_ibfk_2` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offer`
--

LOCK TABLES `offer` WRITE;
/*!40000 ALTER TABLE `offer` DISABLE KEYS */;
INSERT INTO `offer` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,2,1),(6,2,2),(7,2,3),(8,2,4),(9,3,1),(10,3,2),(11,3,3),(12,3,4),(13,4,1),(14,4,2),(15,4,3),(16,4,4),(17,5,1),(18,5,2),(19,5,3),(20,5,4),(21,6,1),(22,6,2),(23,6,3),(24,6,4),(25,7,1),(26,7,2),(27,7,3),(28,7,4),(29,8,1),(30,8,2),(31,8,3),(32,8,4),(33,9,1),(34,9,2),(35,9,3),(36,9,4);
/*!40000 ALTER TABLE `offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'BLR','Belarus'),(2,'FRA','France'),(3,'ITA','Italy');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `hotel_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel`
--

LOCK TABLES `hotel` WRITE;
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` VALUES (1,1,'BHM','Belarus Hotel'),(2,1,'VHM','Victoria Hotel'),(3,1,'PHM','President Hotel'),(4,2,'SHG','Semashko Hotel'),(5,2,'NG','Neman'),(6,3,'HDLP','Hotel Du Louvre'),(7,3,'NPP','Napoleon Paris'),(8,4,'BRN','Beau Rivage'),(9,4,'HNN','Hotel Negresco');
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_type`
--

DROP TABLE IF EXISTS `meal_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meal_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_type`
--

LOCK TABLES `meal_type` WRITE;
/*!40000 ALTER TABLE `meal_type` DISABLE KEYS */;
INSERT INTO `meal_type` VALUES (1,'BB','Breakfast'),(2,'AAL','All Inclusive +');
/*!40000 ALTER TABLE `meal_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offer_availability`
--

DROP TABLE IF EXISTS `offer_availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offer_availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) NOT NULL,
  `5875` int(3) NOT NULL DEFAULT '0',
  `5876` int(3) NOT NULL DEFAULT '0',
  `5877` int(3) NOT NULL DEFAULT '0',
  `5878` int(3) NOT NULL DEFAULT '0',
  `5879` int(3) NOT NULL DEFAULT '0',
  `5880` int(3) NOT NULL DEFAULT '0',
  `5881` int(3) NOT NULL DEFAULT '0',
  `5882` int(3) NOT NULL DEFAULT '0',
  `5883` int(3) NOT NULL DEFAULT '0',
  `5884` int(3) NOT NULL DEFAULT '0',
  `5885` int(3) NOT NULL DEFAULT '0',
  `5886` int(3) NOT NULL DEFAULT '0',
  `5887` int(3) NOT NULL DEFAULT '0',
  `5888` int(3) NOT NULL DEFAULT '0',
  `5889` int(3) NOT NULL DEFAULT '0',
  `5890` int(3) NOT NULL DEFAULT '0',
  `5891` int(3) NOT NULL DEFAULT '0',
  `5892` int(3) NOT NULL DEFAULT '0',
  `5893` int(3) NOT NULL DEFAULT '0',
  `5894` int(3) NOT NULL DEFAULT '0',
  `5895` int(3) NOT NULL DEFAULT '0',
  `5896` int(3) NOT NULL DEFAULT '0',
  `5897` int(3) NOT NULL DEFAULT '0',
  `5898` int(3) NOT NULL DEFAULT '0',
  `5899` int(3) NOT NULL DEFAULT '0',
  `5900` int(3) NOT NULL DEFAULT '0',
  `5901` int(3) NOT NULL DEFAULT '0',
  `5902` int(3) NOT NULL DEFAULT '0',
  `5903` int(3) NOT NULL DEFAULT '0',
  `5904` int(3) NOT NULL DEFAULT '0',
  `5905` int(3) NOT NULL DEFAULT '0',
  `5906` int(3) NOT NULL DEFAULT '0',
  `5907` int(3) NOT NULL DEFAULT '0',
  `5908` int(3) NOT NULL DEFAULT '0',
  `5909` int(3) NOT NULL DEFAULT '0',
  `5910` int(3) NOT NULL DEFAULT '0',
  `5911` int(3) NOT NULL DEFAULT '0',
  `5912` int(3) NOT NULL DEFAULT '0',
  `5913` int(3) NOT NULL DEFAULT '0',
  `5914` int(3) NOT NULL DEFAULT '0',
  `5915` int(3) NOT NULL DEFAULT '0',
  `5916` int(3) NOT NULL DEFAULT '0',
  `5917` int(3) NOT NULL DEFAULT '0',
  `5918` int(3) NOT NULL DEFAULT '0',
  `5919` int(3) NOT NULL DEFAULT '0',
  `5920` int(3) NOT NULL DEFAULT '0',
  `5921` int(3) NOT NULL DEFAULT '0',
  `5922` int(3) NOT NULL DEFAULT '0',
  `5923` int(3) NOT NULL DEFAULT '0',
  `5924` int(3) NOT NULL DEFAULT '0',
  `5925` int(3) NOT NULL DEFAULT '0',
  `5926` int(3) NOT NULL DEFAULT '0',
  `5927` int(3) NOT NULL DEFAULT '0',
  `5928` int(3) NOT NULL DEFAULT '0',
  `5929` int(3) NOT NULL DEFAULT '0',
  `5930` int(3) NOT NULL DEFAULT '0',
  `5931` int(3) NOT NULL DEFAULT '0',
  `5932` int(3) NOT NULL DEFAULT '0',
  `5933` int(3) NOT NULL DEFAULT '0',
  `5934` int(3) NOT NULL DEFAULT '0',
  `5935` int(3) NOT NULL DEFAULT '0',
  `5936` int(3) NOT NULL DEFAULT '0',
  `5937` int(3) NOT NULL DEFAULT '0',
  `5938` int(3) NOT NULL DEFAULT '0',
  `5939` int(3) NOT NULL DEFAULT '0',
  `5940` int(3) NOT NULL DEFAULT '0',
  `5941` int(3) NOT NULL DEFAULT '0',
  `5942` int(3) NOT NULL DEFAULT '0',
  `5943` int(3) NOT NULL DEFAULT '0',
  `5944` int(3) NOT NULL DEFAULT '0',
  `5945` int(3) NOT NULL DEFAULT '0',
  `5946` int(3) NOT NULL DEFAULT '0',
  `5947` int(3) NOT NULL DEFAULT '0',
  `5948` int(3) NOT NULL DEFAULT '0',
  `5949` int(3) NOT NULL DEFAULT '0',
  `5950` int(3) NOT NULL DEFAULT '0',
  `5951` int(3) NOT NULL DEFAULT '0',
  `5952` int(3) NOT NULL DEFAULT '0',
  `5953` int(3) NOT NULL DEFAULT '0',
  `5954` int(3) NOT NULL DEFAULT '0',
  `5955` int(3) NOT NULL DEFAULT '0',
  `5956` int(3) NOT NULL DEFAULT '0',
  `5957` int(3) NOT NULL DEFAULT '0',
  `5958` int(3) NOT NULL DEFAULT '0',
  `5959` int(3) NOT NULL DEFAULT '0',
  `5960` int(3) NOT NULL DEFAULT '0',
  `5961` int(3) NOT NULL DEFAULT '0',
  `5962` int(3) NOT NULL DEFAULT '0',
  `5963` int(3) NOT NULL DEFAULT '0',
  `5964` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `offer_id` (`offer_id`),
  CONSTRAINT `offer_availability_ibfk_1` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offer_availability`
--

LOCK TABLES `offer_availability` WRITE;
/*!40000 ALTER TABLE `offer_availability` DISABLE KEYS */;
INSERT INTO `offer_availability` VALUES (1,1,3,3,3,3,0,0,0,2,2,3,2,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(2,2,1,1,1,1,0,0,0,3,2,2,2,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(3,3,1,1,1,1,1,1,2,4,2,1,2,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(4,4,3,3,34,3,2,0,0,1,1,2,2,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `offer_availability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_type`
--

DROP TABLE IF EXISTS `room_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `guest_count` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_type`
--

LOCK TABLES `room_type` WRITE;
/*!40000 ALTER TABLE `room_type` DISABLE KEYS */;
INSERT INTO `room_type` VALUES (1,'SNG','Single',1),(2,'TWB','Twin Room',2),(3,'DWB','Double Room',2),(4,'TRP','Triple',3);
/*!40000 ALTER TABLE `room_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `guest_count` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `offer_id` (`offer_id`),
  CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-08 16:10:26
