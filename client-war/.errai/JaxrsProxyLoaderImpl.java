package org.jboss.errai.enterprise.client.jaxrs;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.URL;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.service.AvailabilityResource;
import com.gp.training.web.shared.service.BookingResource;
import com.gp.training.web.shared.service.DictionaryResource;
import java.util.List;
import org.jboss.errai.common.client.api.ErrorCallback;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.common.client.framework.ProxyProvider;
import org.jboss.errai.common.client.framework.RemoteServiceProxyFactory;

public class JaxrsProxyLoaderImpl implements JaxrsProxyLoader { public void loadProxies() {
    class com_gp_training_web_shared_service_DictionaryResourceImpl extends AbstractJaxrsProxy implements DictionaryResource {
      private RemoteCallback remoteCallback;
      private ErrorCallback errorCallback;
      public com_gp_training_web_shared_service_DictionaryResourceImpl() {

      }

      public RemoteCallback getRemoteCallback() {
        return remoteCallback;
      }

      public void setRemoteCallback(RemoteCallback callback) {
        remoteCallback = callback;
      }

      public ErrorCallback getErrorCallback() {
        return errorCallback;
      }

      public void setErrorCallback(ErrorCallback callback) {
        errorCallback = callback;
      }

      public List getCountries() {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("dictionary/country");
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, List.class, CountryProxy.class);
          }
        });
        return null;
      }

      public List getCities() {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("dictionary/city");
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, List.class, CityProxy.class);
          }
        });
        return null;
      }

      public List getCitiesByCountryId(final Long a0) {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("dictionary/country/{countryId}/city".replace("{countryId}", URL.encodePathSegment(a0 == null ? "" : a0.toString())));
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, List.class, CityProxy.class);
          }
        });
        return null;
      }
    }
    RemoteServiceProxyFactory.addRemoteProxy(DictionaryResource.class, new ProxyProvider() {
      public Object getProxy() {
        return new com_gp_training_web_shared_service_DictionaryResourceImpl();
      }
    });
    class com_gp_training_web_shared_service_BookingResourceImpl extends AbstractJaxrsProxy implements BookingResource {
      private RemoteCallback remoteCallback;
      private ErrorCallback errorCallback;
      public com_gp_training_web_shared_service_BookingResourceImpl() {

      }

      public RemoteCallback getRemoteCallback() {
        return remoteCallback;
      }

      public void setRemoteCallback(RemoteCallback callback) {
        remoteCallback = callback;
      }

      public ErrorCallback getErrorCallback() {
        return errorCallback;
      }

      public void setErrorCallback(ErrorCallback callback) {
        errorCallback = callback;
      }

      public BookingProxy create(final long a0, final int a1, final String a2, final String a3) {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("booking").append("?").append("endDate").append("=").append(URL.encodeQueryString(a3 == null ? "" : a3)).append("&").append("offerId").append("=").append(URL.encodeQueryString(new Long(a0).toString())).append("&").append("guestCount").append("=").append(URL.encodeQueryString(new Integer(a1).toString())).append("&").append("startDate").append("=").append(URL.encodeQueryString(a2 == null ? "" : a2));
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.POST, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, BookingProxy.class, null);
          }
        });
        return null;
      }

      public BookingProxy addGuests(final int a0, final List<CustomerProxy> a1) {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("booking/{bookingId}".replace("{bookingId}", URL.encodePathSegment(new Integer(a0).toString())));
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.PUT, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        requestBuilder.setHeader("Content-Type", "application/json");
        sendRequest(requestBuilder, MarshallingWrapper.toJSON(a1), new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, BookingProxy.class, null);
          }
        });
        return null;
      }

      public BookingProxy findBookingById(final int a0) {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("booking/{bookingId}".replace("{bookingId}", URL.encodePathSegment(new Integer(a0).toString())));
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, BookingProxy.class, null);
          }
        });
        return null;
      }
    }
    RemoteServiceProxyFactory.addRemoteProxy(BookingResource.class, new ProxyProvider() {
      public Object getProxy() {
        return new com_gp_training_web_shared_service_BookingResourceImpl();
      }
    });
    class com_gp_training_web_shared_service_AvailabilityResourceImpl extends AbstractJaxrsProxy implements AvailabilityResource {
      private RemoteCallback remoteCallback;
      private ErrorCallback errorCallback;
      public com_gp_training_web_shared_service_AvailabilityResourceImpl() {

      }

      public RemoteCallback getRemoteCallback() {
        return remoteCallback;
      }

      public void setRemoteCallback(RemoteCallback callback) {
        remoteCallback = callback;
      }

      public ErrorCallback getErrorCallback() {
        return errorCallback;
      }

      public void setErrorCallback(ErrorCallback callback) {
        errorCallback = callback;
      }

      public List getOffers(final long a0, final int a1, final String a2, final String a3) {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("search").append("?").append("endDate").append("=").append(URL.encodeQueryString(a3 == null ? "" : a3)).append("&").append("guestCount").append("=").append(URL.encodeQueryString(new Integer(a1).toString())).append("&").append("cityId").append("=").append(URL.encodeQueryString(new Long(a0).toString())).append("&").append("startDate").append("=").append(URL.encodeQueryString(a2 == null ? "" : a2));
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, List.class, OfferProxy.class);
          }
        });
        return null;
      }
    }
    RemoteServiceProxyFactory.addRemoteProxy(AvailabilityResource.class, new ProxyProvider() {
      public Object getProxy() {
        return new com_gp_training_web_shared_service_AvailabilityResourceImpl();
      }
    });
  }
}