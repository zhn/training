package org.jboss.errai.marshalling.client.api;

import com.gp.training.web.shared.model.HotelProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.model.RoomTypeProxy;
import org.jboss.errai.marshalling.client.Marshalling;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;

public class Marshaller_c_g_t_w_s_m_OfferProxy_Impl implements GeneratedMarshaller<OfferProxy> {
  private OfferProxy[] EMPTY_ARRAY = new OfferProxy[0];
  private Marshaller<Integer> java_lang_Integer = Marshalling.getMarshaller(Integer.class);
  private Marshaller<Double> java_lang_Double = Marshalling.getMarshaller(Double.class);
  private Marshaller<HotelProxy> com_gp_training_web_shared_model_HotelProxy = null;
  private Marshaller<RoomTypeProxy> com_gp_training_web_shared_model_RoomTypeProxy = null;
  public OfferProxy[] getEmptyArray() {
    return EMPTY_ARRAY;
  }

  public OfferProxy demarshall(EJValue a0, MarshallingSession a1) {
    lazyInit();
    EJObject obj = a0.isObject();
    if (obj == null) {
      return null;
    }
    String objId = obj.get("^ObjectID").isString().stringValue();
    if (a1.hasObject(objId)) {
      return a1.getObject(OfferProxy.class, objId);
    }
    final Integer c0 = java_lang_Integer.demarshall(obj.get("offerId"), a1);
    final Double c1 = java_lang_Double.demarshall(obj.get("price"), a1);
    final HotelProxy c2 = com_gp_training_web_shared_model_HotelProxy.demarshall(obj.get("hotel"), a1);
    final RoomTypeProxy c3 = com_gp_training_web_shared_model_RoomTypeProxy.demarshall(obj.get("room"), a1);
    OfferProxy entity = new OfferProxy(c0, c1, c2, c3);
    a1.recordObject(objId, entity);
    return entity;
  }

  public String marshall(OfferProxy a0, MarshallingSession a1) {
    lazyInit();
    if (a0 == null) {
      return "null";
    }
    final boolean ref = a1.hasObject(a0);
    final StringBuilder json = new StringBuilder("{\"^EncodedType\":\"com.gp.training.web.shared.model.OfferProxy\",\"^ObjectID\"");
    json.append(":\"").append(a1.getObject(a0)).append("\"");
    if (ref) {
      return json.append("}").toString();
    }
    return json.append(",").append("\"offerId\":").append(java_lang_Integer.marshall(a0.getOfferId(), a1)).append(",").append("\"price\":").append(java_lang_Double.marshall(a0.getPrice(), a1)).append(",").append("\"hotel\":").append(com_gp_training_web_shared_model_HotelProxy.marshall(a0.getHotel(), a1)).append(",").append("\"room\":").append(com_gp_training_web_shared_model_RoomTypeProxy.marshall(a0.getRoom(), a1)).append("}").toString();
  }

  private void lazyInit() {
    if (com_gp_training_web_shared_model_HotelProxy == null) {
      com_gp_training_web_shared_model_HotelProxy = Marshalling.getMarshaller(HotelProxy.class);
    }
    if (com_gp_training_web_shared_model_RoomTypeProxy == null) {
      com_gp_training_web_shared_model_RoomTypeProxy = Marshalling.getMarshaller(RoomTypeProxy.class);
    }
  }
}