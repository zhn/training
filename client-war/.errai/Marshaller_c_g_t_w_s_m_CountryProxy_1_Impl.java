package org.jboss.errai.marshalling.client.api;

import com.gp.training.web.shared.model.CountryProxy;
import org.jboss.errai.marshalling.client.Marshalling;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;

public class Marshaller_c_g_t_w_s_m_CountryProxy_1_Impl implements GeneratedMarshaller<CountryProxy> {
  private CountryProxy[] EMPTY_ARRAY = new CountryProxy[0];
  private Marshaller<Integer> java_lang_Integer = Marshalling.getMarshaller(Integer.class);
  private Marshaller<String> java_lang_String = Marshalling.getMarshaller(String.class);
  public CountryProxy[] getEmptyArray() {
    return EMPTY_ARRAY;
  }

  public CountryProxy demarshall(EJValue a0, MarshallingSession a1) {
    lazyInit();
    EJObject obj = a0.isObject();
    if (obj == null) {
      return null;
    }
    String objId = obj.get("^ObjectID").isString().stringValue();
    if (a1.hasObject(objId)) {
      return a1.getObject(CountryProxy.class, objId);
    }
    CountryProxy entity = new CountryProxy();
    a1.recordObject(objId, entity);
    if ((obj.containsKey("id")) && (!obj.get("id").isNull())) {
      entity.setId(java_lang_Integer.demarshall(obj.get("id"), a1));
    }
    if ((obj.containsKey("code")) && (!obj.get("code").isNull())) {
      entity.setCode(java_lang_String.demarshall(obj.get("code"), a1));
    }
    if ((obj.containsKey("name")) && (!obj.get("name").isNull())) {
      entity.setName(java_lang_String.demarshall(obj.get("name"), a1));
    }
    return entity;
  }

  public String marshall(CountryProxy a0, MarshallingSession a1) {
    lazyInit();
    if (a0 == null) {
      return "null";
    }
    final boolean ref = a1.hasObject(a0);
    final StringBuilder json = new StringBuilder("{\"^EncodedType\":\"com.gp.training.web.shared.model.CountryProxy\",\"^ObjectID\"");
    json.append(":\"").append(a1.getObject(a0)).append("\"");
    if (ref) {
      return json.append("}").toString();
    }
    return json.append(",").append("\"id\":").append(java_lang_Integer.marshall(a0.getId(), a1)).append(",").append("\"code\":").append(java_lang_String.marshall(a0.getCode(), a1)).append(",").append("\"name\":").append(java_lang_String.marshall(a0.getName(), a1)).append("}").toString();
  }

  private void lazyInit() {

  }
}