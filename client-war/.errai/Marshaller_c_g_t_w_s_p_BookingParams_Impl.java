package org.jboss.errai.marshalling.client.api;

import com.gp.training.web.shared.params.BookingParams;
import org.jboss.errai.marshalling.client.Marshalling;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;

public class Marshaller_c_g_t_w_s_p_BookingParams_Impl implements GeneratedMarshaller<BookingParams> {
  private BookingParams[] EMPTY_ARRAY = new BookingParams[0];
  private Marshaller<Integer> java_lang_Integer = Marshalling.getMarshaller(Integer.class);
  private Marshaller<String> java_lang_String = Marshalling.getMarshaller(String.class);
  public BookingParams[] getEmptyArray() {
    return EMPTY_ARRAY;
  }

  public BookingParams demarshall(EJValue a0, MarshallingSession a1) {
    lazyInit();
    EJObject obj = a0.isObject();
    if (obj == null) {
      return null;
    }
    String objId = obj.get("^ObjectID").isString().stringValue();
    if (a1.hasObject(objId)) {
      return a1.getObject(BookingParams.class, objId);
    }
    BookingParams entity = new BookingParams();
    a1.recordObject(objId, entity);
    if ((obj.containsKey("guestCount")) && (!obj.get("guestCount").isNull())) {
      entity.setGuestCount(java_lang_Integer.demarshall(obj.get("guestCount"), a1));
    }
    if ((obj.containsKey("offerId")) && (!obj.get("offerId").isNull())) {
      entity.setOfferId(java_lang_Integer.demarshall(obj.get("offerId"), a1));
    }
    if ((obj.containsKey("startDate")) && (!obj.get("startDate").isNull())) {
      entity.setStartDate(java_lang_String.demarshall(obj.get("startDate"), a1));
    }
    if ((obj.containsKey("endDate")) && (!obj.get("endDate").isNull())) {
      entity.setEndDate(java_lang_String.demarshall(obj.get("endDate"), a1));
    }
    return entity;
  }

  public String marshall(BookingParams a0, MarshallingSession a1) {
    lazyInit();
    if (a0 == null) {
      return "null";
    }
    final boolean ref = a1.hasObject(a0);
    final StringBuilder json = new StringBuilder("{\"^EncodedType\":\"com.gp.training.web.shared.params.BookingParams\",\"^ObjectID\"");
    json.append(":\"").append(a1.getObject(a0)).append("\"");
    if (ref) {
      return json.append("}").toString();
    }
    return json.append(",").append("\"guestCount\":").append(java_lang_Integer.marshall(a0.getGuestCount(), a1)).append(",").append("\"offerId\":").append(java_lang_Integer.marshall(a0.getOfferId(), a1)).append(",").append("\"startDate\":").append(java_lang_String.marshall(a0.getStartDate(), a1)).append(",").append("\"endDate\":").append(java_lang_String.marshall(a0.getEndDate(), a1)).append("}").toString();
  }

  private void lazyInit() {

  }
}