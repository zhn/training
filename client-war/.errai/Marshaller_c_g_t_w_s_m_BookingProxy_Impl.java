package org.jboss.errai.marshalling.client.api;

import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.OfferProxy;
import java.util.Date;
import java.util.List;
import org.jboss.errai.marshalling.client.Marshalling;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;

public class Marshaller_c_g_t_w_s_m_BookingProxy_Impl implements GeneratedMarshaller<BookingProxy> {
  private BookingProxy[] EMPTY_ARRAY = new BookingProxy[0];
  private Marshaller<Integer> java_lang_Integer = Marshalling.getMarshaller(Integer.class);
  private Marshaller<OfferProxy> com_gp_training_web_shared_model_OfferProxy = null;
  private Marshaller<Date> java_util_Date = Marshalling.getMarshaller(Date.class);
  private Marshaller<List> java_util_List = Marshalling.getMarshaller(List.class);
  public BookingProxy[] getEmptyArray() {
    return EMPTY_ARRAY;
  }

  public BookingProxy demarshall(EJValue a0, MarshallingSession a1) {
    lazyInit();
    EJObject obj = a0.isObject();
    if (obj == null) {
      return null;
    }
    String objId = obj.get("^ObjectID").isString().stringValue();
    if (a1.hasObject(objId)) {
      return a1.getObject(BookingProxy.class, objId);
    }
    final Integer c0 = java_lang_Integer.demarshall(obj.get("bookingId"), a1);
    final OfferProxy c1 = com_gp_training_web_shared_model_OfferProxy.demarshall(obj.get("offer"), a1);
    final Date c2 = java_util_Date.demarshall(obj.get("startDate"), a1);
    final Date c3 = java_util_Date.demarshall(obj.get("endDate"), a1);
    final Integer c4 = java_lang_Integer.demarshall(obj.get("guestCount"), a1);
    a1.setAssumedElementType("com.gp.training.web.shared.model.CustomerProxy");
    final List c5 = java_util_List.demarshall(obj.get("guests"), a1);
    BookingProxy entity = new BookingProxy(c0, c1, c2, c3, c4, c5);
    a1.recordObject(objId, entity);
    return entity;
  }

  public String marshall(BookingProxy a0, MarshallingSession a1) {
    lazyInit();
    if (a0 == null) {
      return "null";
    }
    final boolean ref = a1.hasObject(a0);
    final StringBuilder json = new StringBuilder("{\"^EncodedType\":\"com.gp.training.web.shared.model.BookingProxy\",\"^ObjectID\"");
    json.append(":\"").append(a1.getObject(a0)).append("\"");
    if (ref) {
      return json.append("}").toString();
    }
    return json.append(",").append("\"bookingId\":").append(java_lang_Integer.marshall(a0.getBookingId(), a1)).append(",").append("\"offer\":").append(com_gp_training_web_shared_model_OfferProxy.marshall(a0.getOffer(), a1)).append(",").append("\"startDate\":").append(java_util_Date.marshall(a0.getStartDate(), a1)).append(",").append("\"endDate\":").append(java_util_Date.marshall(a0.getEndDate(), a1)).append(",").append("\"guestCount\":").append(java_lang_Integer.marshall(a0.getGuestCount(), a1)).append(",").append("\"guests\":").append(java_util_List.marshall(a0.getGuests(), a1)).append("}").toString();
  }

  private void lazyInit() {
    if (com_gp_training_web_shared_model_OfferProxy == null) {
      com_gp_training_web_shared_model_OfferProxy = Marshalling.getMarshaller(OfferProxy.class);
    }
  }
}