package org.jboss.errai.marshalling.client.api;

import com.gp.training.web.shared.model.CustomerProxy;
import org.jboss.errai.marshalling.client.Marshalling;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;

public class Marshaller_c_g_t_w_s_m_CustomerProxy_1_Impl implements GeneratedMarshaller<CustomerProxy> {
  private CustomerProxy[] EMPTY_ARRAY = new CustomerProxy[0];
  private Marshaller<Integer> java_lang_Integer = Marshalling.getMarshaller(Integer.class);
  private Marshaller<String> java_lang_String = Marshalling.getMarshaller(String.class);
  public CustomerProxy[] getEmptyArray() {
    return EMPTY_ARRAY;
  }

  public CustomerProxy demarshall(EJValue a0, MarshallingSession a1) {
    lazyInit();
    EJObject obj = a0.isObject();
    if (obj == null) {
      return null;
    }
    String objId = obj.get("^ObjectID").isString().stringValue();
    if (a1.hasObject(objId)) {
      return a1.getObject(CustomerProxy.class, objId);
    }
    final Integer c0 = java_lang_Integer.demarshall(obj.get("customerId"), a1);
    final String c1 = java_lang_String.demarshall(obj.get("firstName"), a1);
    final String c2 = java_lang_String.demarshall(obj.get("lastName"), a1);
    final String c3 = java_lang_String.demarshall(obj.get("email"), a1);
    CustomerProxy entity = new CustomerProxy(c0, c1, c2, c3);
    a1.recordObject(objId, entity);
    return entity;
  }

  public String marshall(CustomerProxy a0, MarshallingSession a1) {
    lazyInit();
    if (a0 == null) {
      return "null";
    }
    final boolean ref = a1.hasObject(a0);
    final StringBuilder json = new StringBuilder("{\"^EncodedType\":\"com.gp.training.web.shared.model.CustomerProxy\",\"^ObjectID\"");
    json.append(":\"").append(a1.getObject(a0)).append("\"");
    if (ref) {
      return json.append("}").toString();
    }
    return json.append(",").append("\"customerId\":").append(java_lang_Integer.marshall(a0.getCustomerId(), a1)).append(",").append("\"firstName\":").append(java_lang_String.marshall(a0.getFirstName(), a1)).append(",").append("\"lastName\":").append(java_lang_String.marshall(a0.getLastName(), a1)).append(",").append("\"email\":").append(java_lang_String.marshall(a0.getEmail(), a1)).append("}").toString();
  }

  private void lazyInit() {

  }
}