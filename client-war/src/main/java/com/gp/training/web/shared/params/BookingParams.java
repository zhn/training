package com.gp.training.web.shared.params;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class BookingParams {

	private int guestCount;
	private int offerId;
	private String startDate;
	private String endDate;

	public BookingParams() {
		
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
