package com.gp.training.web.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.model.HotelDTO;
import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;
import com.gp.training.bizlogic.client.resource.BookingService;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.model.HotelProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.model.RoomTypeProxy;
import com.gp.training.web.shared.service.BookingResource;

@Component
public class BookingResourceImpl implements BookingResource {

	@Autowired
	private BookingService bookingService;

	@Override
	public BookingProxy create(long offerId, int guestCount, String startDate, String endDate) {
		BookingDTO bookingDTO = bookingService.create(offerId, guestCount, startDate, endDate);
		BookingProxy bookingProxy = convertBookingDtoToProxy(bookingDTO);
		return bookingProxy;
	}

	@Override
	public BookingProxy addGuests(int bookingId, List<CustomerProxy> guests) {

		List<CustomerDTO> guestsDTO = new ArrayList<>();
		for (CustomerProxy proxy : guests) {
			CustomerDTO dto = new CustomerDTO();
			dto.setFirstName(proxy.getFirstName());
			dto.setLastName(proxy.getLastName());
			dto.setEmail(proxy.getEmail());
			guestsDTO.add(dto);
		}
		BookingDTO bookingDTO = bookingService.addGuests(bookingId, guestsDTO);
		BookingProxy bookingProxy = convertBookingDtoToProxy(bookingDTO);
		return bookingProxy;
	}

	@Override
	public BookingProxy findBookingById(int bookingId) {
		BookingDTO bookingDTO = bookingService.findBookingById(bookingId);
		BookingProxy bookingProxy = convertBookingDtoToProxy(bookingDTO);
		return bookingProxy;
	}

	private BookingProxy convertBookingDtoToProxy(BookingDTO bookingDTO) {
		BookingProxy bookingProxy = new BookingProxy();

		if (bookingDTO != null) {
			bookingProxy.setBookingId(bookingDTO.getId().intValue());
			bookingProxy.setStartDate(bookingDTO.getStartDate());
			bookingProxy.setEndDate(bookingDTO.getEndDate());
			bookingProxy.setGuestCount(bookingDTO.getGuestCount());

			
			if (bookingDTO.getGuestsDTO() != null) {
				List<CustomerDTO> customersDTO = bookingDTO.getGuestsDTO();
				List<CustomerProxy> customersProxy = new ArrayList<>();
				for (CustomerDTO customerDTO : customersDTO) {
					CustomerProxy customerProxy = new CustomerProxy();
					customerProxy.setCustomerId(customerDTO.getId().intValue());
					customerProxy.setFirstName(customerDTO.getFirstName());
					customerProxy.setLastName(customerDTO.getLastName());
					customerProxy.setEmail(customerDTO.getEmail());
					customersProxy.add(customerProxy);
				}
				bookingProxy.setGuests(customersProxy);
			}
			

			OfferProxy offerProxy = new OfferProxy();
			if (bookingDTO.getOfferDTO() != null) {
				OfferDTO offerDTO = bookingDTO.getOfferDTO();
				offerProxy.setOfferId(offerDTO.getOfferId().intValue());
				RoomTypeProxy roomTypeProxy = new RoomTypeProxy();
				if (offerDTO.getRoom() != null) {
					RoomTypeDTO roomTypeDTO = offerDTO.getRoom();
					roomTypeProxy.setId(roomTypeDTO.getId().intValue());
					roomTypeProxy.setCode(roomTypeDTO.getCode());
					roomTypeProxy.setName(roomTypeDTO.getName());
					roomTypeProxy.setGuestCount(roomTypeDTO.getGuestCount());
				}
				offerProxy.setRoom(roomTypeProxy);

				HotelProxy hotelProxy = new HotelProxy();
				if (offerDTO.getHotel() != null) {
					HotelDTO hotelDTO = offerDTO.getHotel();
					hotelProxy.setId(hotelDTO.getId().intValue());
					hotelProxy.setCode(hotelDTO.getCode());
					hotelProxy.setName(hotelDTO.getName());
				}
				offerProxy.setHotel(hotelProxy);
			}
			bookingProxy.setOffer(offerProxy);
		}
		return bookingProxy;
	}

}
