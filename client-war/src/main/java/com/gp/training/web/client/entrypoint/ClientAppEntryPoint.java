package com.gp.training.web.client.entrypoint;

import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.NavigationService;
import com.gp.training.web.client.style.Resources;
import com.sksamuel.jqm4gwt.JQMContext;
import com.sksamuel.jqm4gwt.ScriptUtils;

public class ClientAppEntryPoint implements EntryPoint {

	private static final String HTTP = "http://";
	private static final String HTTPS = "https://";

	@Override
	public void onModuleLoad() {
		ScriptUtils.waitJqmLoaded(new Callback<Void, Throwable>() {
			@Override
			public void onSuccess(Void result) {
				RestClient.setApplicationRoot(getApplicationURL() + "rest");
				bootstrap();
				buildAppContext();
			}

			@Override
			public void onFailure(Throwable reason) {
				Window.alert(reason.getMessage());
			}
		});
	}

	public static String getApplicationURL() {
		String s = GWT.getHostPageBaseURL();
		if (s == null || s.isEmpty())
			return "";
		if (s.startsWith(HTTP))
			s = s.substring(HTTP.length());
		else if (s.startsWith(HTTPS))
			s = s.substring(HTTPS.length());
		int p = s.indexOf('/');
		if (p == -1)
			return "";
		s = s.substring(p).trim();
		return s;
	}

	public static void bootstrap() {
		Resources.r.styles().ensureInjected();
	}

	private void buildAppContext() {

		JQMContext.disableHashListening();
		AppContext.init();
		AppContext.navigationService.next(NavigationService.START_PAGE, null);
	}
}
