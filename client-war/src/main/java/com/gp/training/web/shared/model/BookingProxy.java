package com.gp.training.web.shared.model;

import java.util.Date;
import java.util.List;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;
@Portable
public class BookingProxy {

	private int bookingId;

	private OfferProxy offer;

	private Date startDate;

	private Date endDate;

	private int guestCount;

	private List<CustomerProxy> guests;

	public BookingProxy() {

	}

	public BookingProxy(@MapsTo("bookingId") int bookingId, @MapsTo("offer") OfferProxy offer,
			@MapsTo("startDate") Date startDate, @MapsTo("endDate") Date endDate, @MapsTo("guestCount") int guestCount,
			@MapsTo("guests") List<CustomerProxy> guests) {
		super();
		this.bookingId = bookingId;
		this.offer = offer;
		this.startDate = startDate;
		this.endDate = endDate;
		this.guestCount = guestCount;
		this.guests = guests;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public OfferProxy getOffer() {
		return offer;
	}

	public void setOffer(OfferProxy offer) {
		this.offer = offer;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public List<CustomerProxy> getGuests() {
		return guests;
	}

	public void setGuests(List<CustomerProxy> guests) {
		this.guests = guests;
	}

	
	

}
