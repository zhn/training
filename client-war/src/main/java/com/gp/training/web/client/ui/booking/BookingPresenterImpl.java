package com.gp.training.web.client.ui.booking;

import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.service.BookingResource;

public class BookingPresenterImpl implements BookingPresenter {

	@Override
	public void findBookingById(int bookingId, final Callback<BookingProxy, Void> callback) {
		RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
			@Override
			public void callback(BookingProxy response) {
				if (callback != null)
					callback.onSuccess(response);
			}
		}).findBookingById(bookingId);

		
	}

}
