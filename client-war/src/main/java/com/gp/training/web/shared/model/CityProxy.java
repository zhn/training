package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class CityProxy {
	private int id;
	private String code;
	private String name;
	private CountryProxy country;

	public CityProxy() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CountryProxy getCountry() {
		return country;
	}

	public void setCountry(CountryProxy country) {
		this.country = country;
	}

}
