package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.shared.model.CustomerProxy;
import com.sksamuel.jqm4gwt.form.elements.JQMText;

public class CustomerItem extends Composite {
	@UiField
	protected JQMText firstName;
	@UiField
	protected JQMText lastName;
	@UiField
	protected JQMText email;

	private static CustomerItemUiBinder uiBinder = GWT.create(CustomerItemUiBinder.class);

	interface CustomerItemUiBinder extends UiBinder<Widget, CustomerItem> {
	}

	public CustomerItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public CustomerProxy getCustomer(){
		CustomerProxy customerProxy = new CustomerProxy();
		customerProxy.setFirstName(firstName.getValue());
		customerProxy.setLastName(lastName.getValue());
		customerProxy.setEmail(email.getValue());
		return customerProxy;
	}

}
