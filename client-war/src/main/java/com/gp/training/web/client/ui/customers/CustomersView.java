package com.gp.training.web.client.ui.customers;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.PageToken;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.customers.customer.CustomerItem;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

public class CustomersView extends GenericBaseView<CustomersPresenter> implements PageBaseView {

	private PageParams pageParams;

	@UiField
	protected JQMButton nextBtn;
	@UiField
	protected JQMList guestList;

	private static CustomersViewUiBinder uiBinder = GWT.create(CustomersViewUiBinder.class);

	interface CustomersViewUiBinder extends UiBinder<Widget, CustomersView> {
	}

	private Callback<BookingProxy, Void> guestsCallback;

	public CustomersView() {
		initWidget(uiBinder.createAndBindUi(this));
		setPresenter(new CustomersPresenterImpl());
		initActions();
		buildGuestsCallback();

	}

	private void buildGuestsCallback() {
		guestsCallback = new Callback<BookingProxy, Void>() {

			@Override
			public void onSuccess(BookingProxy result) {
				PageParams newParams = new PageParams();
				newParams.setBookingId(result.getBookingId());
				AppContext.navigationService.next(PageToken.BOOKING, newParams);
			}

			@Override
			public void onFailure(Void reason) {
			}
		};
	}

	private void initActions() {
		nextBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int bookingId = pageParams.getBookingId();
				List<CustomerProxy> guests = getGuestList();
				getPresenter().addGuests(bookingId, guests, guestsCallback);
				/*
				 * PageParams newParams = new PageParams();
				 * AppContext.navigationService.next(PageToken.BOOKING,
				 * newParams);
				 */
			}
		});
	}

	protected List<CustomerProxy> getGuestList() {
		List<JQMListItem> listItems = guestList.getItems();
		List<CustomerProxy> customerProxies = new ArrayList<>();
		for (JQMListItem jqmListItem : listItems) {
			int widgetCount = jqmListItem.getWidgetCount();
			for (int i = 0; i < widgetCount; i++) {
				Widget childWidget = jqmListItem.getWidget(i);
				if (childWidget instanceof CustomerItem) {
					CustomerProxy customerProxy = ((CustomerItem) childWidget).getCustomer();
					customerProxies.add(customerProxy);
				}
			}
		}
		return customerProxies;
	}

	@Override
	public void loadPageParams(PageParams params) {
		pageParams = params != null ? params : new PageParams();
		recreateGuestsList();
	}

	private void recreateGuestsList() {
		guestList.clear();
		int guestCount = pageParams.getGuestCount();
		for (int i = 0; i < guestCount; i++) {
			JQMListItem listItem = new JQMListItem();
			listItem.setControlGroup(true, false);
			CustomerItem customer = new CustomerItem();
			listItem.addWidget(customer);
			guestList.appendItem(listItem);
		}
		guestList.recreate();
		guestList.refresh();
	}
}
