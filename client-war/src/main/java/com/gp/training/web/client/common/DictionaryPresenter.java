package com.gp.training.web.client.common;

import java.util.ArrayList;
import java.util.List;

import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.service.DictionaryResource;

public class DictionaryPresenter {

	public List<CountryProxy> countries = new ArrayList<>();

	public void getCountries(Callback<List<CountryProxy>, Void> originCallback) {

		if (countries.size() > 0) {
			originCallback.onSuccess(countries);
		} else {
			loadCountries(originCallback);
		}
	}

	private void loadCountries(final Callback<List<CountryProxy>, Void> originCallback) {
		RemoteCallback<List<CountryProxy>> callback = new RemoteCallback<List<CountryProxy>>() {
			@Override
			public void callback(List<CountryProxy> response) {
				if (response != null && response.size() > 0) {
					countries.addAll(response);
					if (originCallback != null)
						originCallback.onSuccess(response);
				}
			}
		};
		RestClient.create(DictionaryResource.class, callback).getCountries();
	}

	public void getCities(Callback<List<CityProxy>, Void> originCallback) {
		loadCities(originCallback);
	}

	private void loadCities(final Callback<List<CityProxy>, Void> originCallback) {
		RemoteCallback<List<CityProxy>> callback = new RemoteCallback<List<CityProxy>>() {
			@Override
			public void callback(List<CityProxy> response) {
				if (response != null && response.size() > 0) {
					if (originCallback != null)
						originCallback.onSuccess(response);
				}
			}
		};
		RestClient.create(DictionaryResource.class, callback).getCities();
	}

	public void getCitiesByCountryId(Long countryId, Callback<List<CityProxy>, Void> originCallback) {
		loadCitiesByCountryId(countryId, originCallback);
	}

	private void loadCitiesByCountryId(Long countryId, final Callback<List<CityProxy>, Void> originCallback) {
		RemoteCallback<List<CityProxy>> callback = new RemoteCallback<List<CityProxy>>() {
			@Override
			public void callback(List<CityProxy> response) {
				if (response != null && response.size() > 0) {
					if (originCallback != null)
						originCallback.onSuccess(response);
				}
			}
		};
		RestClient.create(DictionaryResource.class, callback).getCitiesByCountryId(countryId);
	}

}
