package com.gp.training.web.client.util;

import java.util.Date;

import com.google.gwt.core.client.GWT;

import com.google.gwt.thirdparty.guava.common.annotations.GwtCompatible;
import com.google.gwt.thirdparty.guava.common.annotations.GwtIncompatible;

public abstract class DateUtils {

	public static final String PATTERN = "yyyy-MM-dd";
	public static DateUtils getFormat() {
		if (GWT.isClient())
			return new DateUtilsClient(PATTERN);
		else
			return new DateUtilsServer(PATTERN);
	}

	public abstract String format(Date date);

	public abstract Date parse(String dateString);

	@GwtCompatible
	private static class DateUtilsClient extends DateUtils {
		protected String pattern;

		public DateUtilsClient(String pattern) {
			this.pattern = pattern;
		}

		public String format(Date date) {
			return com.google.gwt.i18n.client.DateTimeFormat.getFormat(pattern).format(date);
		}

		public Date parse(String stringDate) {
			return com.google.gwt.i18n.client.DateTimeFormat.getFormat(pattern).parseStrict(stringDate);
		}
	}

	private static class DateUtilsServer extends DateUtilsClient {

		public DateUtilsServer(String pattern) {
			super(pattern);
		}

		@GwtIncompatible("Server format")
		public String format(Date date) {
			return (new java.text.SimpleDateFormat(pattern)).format(date);
		}

		@GwtIncompatible("Server parse")
		public Date parse(String dateString) {
			try {
				return (new java.text.SimpleDateFormat(pattern)).parse(dateString);
			} catch (Exception ex) {
				throw new IllegalArgumentException("Cannot convert to date: " + dateString);
			}
		}

	}
}
