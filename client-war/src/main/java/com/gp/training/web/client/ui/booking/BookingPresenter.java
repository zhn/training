package com.gp.training.web.client.ui.booking;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingProxy;

public interface BookingPresenter  extends Presenter{

	void findBookingById(int bookingId, Callback<BookingProxy, Void> findCallback);

}
