package com.gp.training.web.client.ui.booking;


import java.util.List;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.PageToken;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.util.DateUtils;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.form.elements.JQMText;
import com.sksamuel.jqm4gwt.html.Div;

public class BookingView extends GenericBaseView<BookingPresenter> implements PageBaseView {

	private PageParams pageParams;

	private static BookingViewUiBinder uiBinder = GWT.create(BookingViewUiBinder.class);

	interface BookingViewUiBinder extends UiBinder<Widget, BookingView> {
	}

	@UiField
	protected Div bookingIdDiv;
	@UiField
	protected Div hotelNameField;
	@UiField
	protected Div roomNameField;
	@UiField
	protected Div startDate;
	@UiField
	protected Div endDate;
	@UiField
	protected Div guestCount;
	@UiField
	protected JQMText bookingId;
	@UiField
	protected JQMButton showBookingBtn;
	@UiField
	protected CellTable<CustomerProxy> guestsTable;

	private Callback<BookingProxy, Void> showBookingCallback;

	public BookingView() {
		initWidget(uiBinder.createAndBindUi(this));
		setPresenter(new BookingPresenterImpl());
		initActions();
		initCellTable();
		buildShowBookingCallback();
	}

	private void initCellTable() {
		TextColumn<CustomerProxy> guestIdColumn = new TextColumn<CustomerProxy>() {
			@Override
			public String getValue(CustomerProxy customerProxy) {
				return String.valueOf(customerProxy.getCustomerId());
			}
		};
		guestsTable.addColumn(guestIdColumn, "#ID");
		TextColumn<CustomerProxy> firstNameColumn = new TextColumn<CustomerProxy>() {
			@Override
			public String getValue(CustomerProxy customerProxy) {
				return customerProxy.getFirstName();
			}
		};
		guestsTable.addColumn(firstNameColumn, "First Name");

		TextColumn<CustomerProxy> lastNameColumn = new TextColumn<CustomerProxy>() {
			@Override
			public String getValue(CustomerProxy customerProxy) {
				return customerProxy.getLastName();
			}
		};
		guestsTable.addColumn(lastNameColumn, "Last Name");

		TextColumn<CustomerProxy> emailColumn = new TextColumn<CustomerProxy>() {
			@Override
			public String getValue(CustomerProxy customerProxy) {
				return customerProxy.getEmail();
			}
		};
		guestsTable.addColumn(emailColumn, "Email");
	}

	private void initActions() {
		showBookingBtn.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				int id = Integer.valueOf(bookingId.getValue());
				PageParams params = new PageParams();
				params.setBookingId(id);
				AppContext.navigationService.next(PageToken.BOOKING, params);
			}
		});
	}

	private void buildShowBookingCallback() {
		showBookingCallback = new Callback<BookingProxy, Void>() {

			@Override
			public void onSuccess(BookingProxy result) {
				if (result != null) {
					populateBookingView(result);
				}
			}

			@Override
			public void onFailure(Void reason) {

			}
		};
	}

	private void populateBookingView(BookingProxy result) {
		bookingIdDiv.setText(String.valueOf(result.getBookingId()));
		hotelNameField.setText(result.getOffer().getHotel().getName());
		roomNameField.setText(result.getOffer().getRoom().getName());
		startDate.setText(DateUtils.getFormat().format(result.getStartDate()));
		endDate.setText(DateUtils.getFormat().format(result.getEndDate()));
		guestCount.setText(String.valueOf(result.getGuestCount()));
		if (result.getGuests() != null) {
			List<CustomerProxy> guests = result.getGuests();
			guestsTable.setRowCount(guests.size(), true);
			guestsTable.setRowData(0, guests);
		}
		
	}
	
	@Override
	public void loadPageParams(PageParams params) {
		if (params != null) {
			pageParams = params;
			int id = pageParams.getBookingId();
			getPresenter().findBookingById(id, showBookingCallback);
		} else {
			pageParams = new PageParams();
		}

	}

}
