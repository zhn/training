package com.gp.training.web.client.ui.customers;

import java.util.List;

import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.service.BookingResource;

public class CustomersPresenterImpl implements CustomersPresenter {

	@Override
	public void addGuests(int bookingId, List<CustomerProxy> guests,
			final Callback<BookingProxy, Void> callback) {

		RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
			@Override
			public void callback(BookingProxy response) {
				if (callback != null)
					callback.onSuccess(response);
			}
		}).addGuests(bookingId, guests);

	}

}
