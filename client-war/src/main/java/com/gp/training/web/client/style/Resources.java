package com.gp.training.web.client.style;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

public interface Resources extends ClientBundle {

	public static Resources r = GWT.create(Resources.class);

	public interface Styles extends CssResource {

		public String floatRight();

		public String border1();

		public String searchPanel();

		public String offersPanel();

		public String btn();

		public String content();

		public String label();

		public String fieldContent();

		public String guestsTable();

		public String guestLabel();

		public String field();

		public String fieldBooking();

		public String bookingText();

		public String showBtn();

		public String block();

		public String nextBtn();

		public String fontWeightBold();

		public String offerDesc();

		public String selectCheckbox();

	}

	@Source({ "css/Style.gss" })
	public Styles styles();
}
