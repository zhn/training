package com.gp.training.web.shared.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;

@Path("/booking")
public interface BookingResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public BookingProxy create(@QueryParam("offerId") long offerId, 
			@QueryParam("guestCount") int guestCount,
			@QueryParam("startDate") String startDate, 
			@QueryParam("endDate") String endDate);

	@PUT
	@Path("/{bookingId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BookingProxy addGuests(@PathParam("bookingId")int bookingId, List<CustomerProxy> guests);

	@GET
	@Path("/{bookingId}")
	@Produces(MediaType.APPLICATION_JSON)
	public BookingProxy findBookingById(@PathParam("bookingId")int bookingId);

}
