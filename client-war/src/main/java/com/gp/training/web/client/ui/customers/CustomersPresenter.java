package com.gp.training.web.client.ui.customers;

import java.util.List;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;


public interface CustomersPresenter extends Presenter{

	void addGuests(int bookingId, List<CustomerProxy> customerProxies, Callback<BookingProxy, Void> callback);

}
