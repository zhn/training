package com.gp.training.web.client.ui.common;

import com.google.gwt.user.client.ui.Composite;

public class GenericBaseView<P> extends Composite{
	private P presenter;
	
	protected P getPresenter() {
		return this.presenter;
	}
	
	protected void setPresenter(P presenter) {
		this.presenter = presenter;
	};
}
