package com.gp.training.web.client.app;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.booking.BookingView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.customers.CustomersView;
import com.gp.training.web.client.ui.search.SearchView;
import com.sksamuel.jqm4gwt.JQMContent;
import com.sksamuel.jqm4gwt.JQMContext;
import com.sksamuel.jqm4gwt.JQMPage;

@Singleton
public class NavigationService {
	
	public static final PageToken START_PAGE = PageToken.SEARCH; 
	
	private Map<PageToken, JQMPage> token2PageMap = new HashMap<>();   
	private Map<JQMPage, PageParams> page2ParamsMap = new HashMap<>();
	
	public NavigationService() {
		initToken2PageMap();
		initPage2ParamsMap();
	}

	private void initToken2PageMap() {
		//1. search
		SearchView searchView = new SearchView();
		JQMPage searchPage = new JQMPage(searchView);
		token2PageMap.put(PageToken.SEARCH, searchPage);
		
		//2. customers
		CustomersView customersView = new CustomersView();
		JQMPage customersPage = new JQMPage(customersView);
		token2PageMap.put(PageToken.CUSTOMERS, customersPage);

		//3. booking
		BookingView bookingView = new BookingView();
		JQMPage bookingPage = new JQMPage(bookingView);
		token2PageMap.put(PageToken.BOOKING, bookingPage);	
	}
	
	private void initPage2ParamsMap() {
		for (JQMPage page : token2PageMap.values()) {
			page2ParamsMap.put(page, new PageParams());
		}
	}
	
	public void next(PageToken pageToken, PageParams pageParams) {
		
		JQMPage targetPage = token2PageMap.get(pageToken);
		page2ParamsMap.put(targetPage, pageParams != null ? pageParams : new PageParams());
		
		updateBaseView(targetPage);
		
		
		JQMContext.changePage(targetPage, false);
	}
	
	private void updateBaseView(JQMPage targetPage) {
		int widgetsCount = targetPage.getWidgetCount();
		
		for (int i=0; i<widgetsCount; i++ ) {
			Widget w = targetPage.getWidget(i);
			
			if (w instanceof JQMContent) {
				JQMContent content = (JQMContent) w;
				int contentWidgetCount = content.getWidgetCount();
				for (int y=0; y<contentWidgetCount; y++) {
					Widget contentChildW = content.getWidget(y);
					if (contentChildW instanceof PageBaseView) {
						((PageBaseView) contentChildW).loadPageParams(page2ParamsMap.get(targetPage));	
						break;
					}
				}
				break;
			}
			
		}	
		
	}
	
}
