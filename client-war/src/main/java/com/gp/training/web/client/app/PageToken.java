package com.gp.training.web.client.app;

public enum PageToken {
	
	SEARCH,
	CUSTOMERS,
	BOOKING
}
