package com.gp.training.web.client.ui.search;

import java.util.List;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.params.BookingParams;
import com.gp.training.web.shared.params.OfferSearchParams;

public interface SearchPresenter extends Presenter {
	public void loadCountries(Callback<List<CountryProxy>, Void> callback);

	public void loadCities(Callback<List<CityProxy>, Void> callback);

	public void loadCitiesByCountryId(Long countryId, Callback<List<CityProxy>, Void> callback);

	public void getOffers(OfferSearchParams params, Callback<List<OfferProxy>, Void> callback);

	public void createBooking(BookingParams bookingParams, Callback<BookingProxy, Void> callback);
}
