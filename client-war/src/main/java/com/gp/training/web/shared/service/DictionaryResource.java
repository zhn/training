package com.gp.training.web.shared.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;


@Path("/dictionary")
public interface DictionaryResource {
	
	@GET
	@Path("country")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountryProxy> getCountries();
	
	@GET
	@Path("city")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityProxy> getCities();
	
	@GET
	@Path("/country/{countryId}/city")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityProxy> getCitiesByCountryId(@PathParam("countryId") Long countryId);

}
