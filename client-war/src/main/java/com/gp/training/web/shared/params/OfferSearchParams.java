package com.gp.training.web.shared.params;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class OfferSearchParams {
	
	private int guestCount;
	private int cityId;
	private String startDate;
	private String endDate;
	
	public OfferSearchParams() {
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
		
}
