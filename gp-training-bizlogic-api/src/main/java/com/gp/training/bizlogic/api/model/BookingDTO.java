package com.gp.training.bizlogic.api.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "booking")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingDTO {

	@XmlAttribute(name = "bookingId")
	private Long id;
	@XmlElement(name = "offer")
	private OfferDTO offerDTO;
	@XmlElement(name = "startDate")
	private Date startDate;
	@XmlElement(name = "endDate")
	private Date endDate;
	@XmlElement(name = "guestCount")
	private int guestCount;

	@XmlElement(name = "guests")
	private List<CustomerDTO> guestsDTO;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OfferDTO getOfferDTO() {
		return offerDTO;
	}

	public void setOfferDTO(OfferDTO offerDTO) {
		this.offerDTO = offerDTO;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public List<CustomerDTO> getGuestsDTO() {
		return guestsDTO;
	}

	public void setGuestsDTO(List<CustomerDTO> guestsDTO) {
		this.guestsDTO = guestsDTO;
	}

	
	
}
