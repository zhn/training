package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.CustomerDTO;

@Path("/booking")
public interface BookingResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public BookingDTO create(@QueryParam("offerId") long offerId, 
			@QueryParam("guestCount") int guestCount,
			@QueryParam("startDate") String startDate, 
			@QueryParam("endDate") String endDate) ;
		

	@PUT
	@Path("/{bookingId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BookingDTO addGuests(@PathParam("bookingId") int bookingId, List<CustomerDTO> guestsDTO);

	@GET
	@Path("/{bookingId}")
	@Produces(MediaType.APPLICATION_JSON)
	public BookingDTO findBookingById(@PathParam("bookingId")int bookingId);

}
