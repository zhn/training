package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;

@Path("/dictionary")
public interface DictionaryResource{

	@GET
	@Path("/roomtype/{roomTypeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public RoomTypeDTO getRoomTypeById(@PathParam("roomTypeId") Long roomTypeId) ;

	@GET
	@Path("/roomtype")
	@Produces(MediaType.APPLICATION_JSON)
	public List<RoomTypeDTO> getAllRoomTypes() ;
	@GET
	@Path("/mealtype/{mealTypeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public MealTypeDTO getMealTypeById(@PathParam("mealTypeId") Long mealTypeId);

	@GET
	@Path("/mealtype")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MealTypeDTO> getAllMealTypes();

	@GET
	@Path("/city/{cityId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CityDTO getCityById(@PathParam("cityId") Long cityId);

	@GET
	@Path("/city")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityDTO> getAllCities() ;

	@GET
	@Path("/country/{countryId}/city")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityDTO> getCitiesByCountryId(@PathParam("countryId") Long countryId);
	
	@GET
	@Path("/country/{countryId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CountryDTO getCountryById(@PathParam("countryId") Long countryId) ;
	
	@GET
	@Path("/country")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountryDTO> getAllCountries() ;


}