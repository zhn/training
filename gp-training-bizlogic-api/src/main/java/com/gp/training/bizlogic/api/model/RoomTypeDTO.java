package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "roomType")
@XmlAccessorType(XmlAccessType.FIELD)
public class RoomTypeDTO {

	@XmlAttribute(name = "roomTypeId")
	private Long id;
	@XmlElement(name = "roomTypeName")
	private String name;
	@XmlElement(name = "roomTypeCode")
	private String code;
	@XmlElement(name = "guestCount")
	private Integer guestCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(Integer guestCount) {
		this.guestCount = guestCount;
	}

}
