package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="hotel")
@XmlAccessorType(XmlAccessType.FIELD)
public class HotelDTO {
	
	@XmlAttribute(name="hotelId")
	private Long id;
	@XmlElement(name="hotelName")
	private String name;
	@XmlElement(name="hotelCode")
	private String code;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
